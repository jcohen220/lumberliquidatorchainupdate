--update chain patterns
UPDATE chain
SET patterns = ARRAY ['Lumber Liquidators%']
WHERE id = 1497376625224845155;

--update place_* tables for place  view

UPDATE place_neustar AS pn
SET chain_id = ch.id
FROM chain ch
WHERE pn.name ILIKE ANY (ch.patterns)
  AND pn.chain_id IS NULL
  AND ch.id = 1497376625224845155;

UPDATE place_custom AS pc
SET chain_id = ch.id
FROM chain ch
WHERE pc.name ILIKE ANY (ch.patterns)
  AND pc.chain_id IS NULL
  AND ch.id = 1497376625224845155;

UPDATE place_google AS pg
SET chain_id = ch.id
FROM chain ch
WHERE name ILIKE ANY (ch.patterns)
  AND pc.chain_id IS NULL
  AND ch.id = 1497376625224845155;

-- update point_of_interest
UPDATE point_of_interest AS poi
SET chain_id = pn.chain_id
FROM place_neustar pn
WHERE pn.neustar_pid::TEXT = poi.external_key
  AND poi.metadata_provider_id = 1
  AND pn.chain_id = 1497376625224845155;

UPDATE point_of_interest AS poi
SET chain_id = pn.chain_id
FROM place_neustar pn
WHERE pn.socialradar_id::TEXT = poi.external_key
  AND poi.metadata_provider_id = 2
  AND pn.chain_id = 1497376625224845155;

UPDATE point_of_interest AS poi
SET chain_id = pc.chain_id
FROM place_custom pc
WHERE pc.id::TEXT = poi.external_key
  AND poi.metadata_provider_id = 3
  AND pc.chain_id = 1497376625224845155;





