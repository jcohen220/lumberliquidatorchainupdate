# Lumber Liquidators updates for RVL-3038

This repo holds queries used to associate Lumber Liquidators places to the already existing LL chain with ID `1497376625224845155`.

## LL place names
The following non-normalized place names currently exist for LL in our system:

* Lumber Liquidators
* Lumber Liquidators Holdings Inc
* Lumber Liquidators Inc
* Lumber Liquidators, Inc.

At the time this was written, these represent 188 places in `RDS` prod (187 in sandbox)

## Tables affected
Since the chain already exists the underlying `place_*` tables that comprise the place view, as well as `point_of_interest` were updated
these updates can be found in the `updates.sql` file.
